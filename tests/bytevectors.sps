#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (srfi :64 testing)
  (industria bytevectors)
  (industria tests runner))

(test-begin "bytevectors-r6rs")

(define-syntax show
  (syntax-rules ()
    [(_ expr)
     (guard (exn
             (else
              (write exn)
              (newline)))
       (write 'expr)
       (display " => ")
       (write expr)
       (newline))]))

;; Simple ocular R6RS conformancy checks
(show (get-bytevector-n (open-bytevector-input-port #vu8()) 0))
(show (get-bytevector-n (open-bytevector-input-port #vu8(1)) 0))
(show (get-string-n (open-string-input-port "") 0))
(show (get-string-n (open-string-input-port "x") 0))

(show (let ((x (make-bytevector 0)))
        (get-bytevector-n! (open-bytevector-input-port #vu8())
                           x 0 0)))
(show (let ((x (make-string 0)))
        (get-string-n! (open-string-input-port "")
                       x 0 0)))
(show (let ((x (make-bytevector 0)))
        (get-bytevector-n! (open-bytevector-input-port #vu8(1))
                           x 0 0)))
(show (let ((x (make-string 0)))
        (get-string-n! (open-string-input-port "x")
                       x 0 0)))

(test-begin "bytevectors")
(test-equal #vu8()
            (uint->bytevector 0))
(test-end)

(test-exit)
