#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (industria ssh private serialize)
  (srfi :64 testing)
  (except (rnrs) put-string)
  (industria tests runner))

(test-begin "ssh-serial-get")
(test-equal #x29b7f4aa
            (let ((p (open-bytevector-input-port #vu8(#x29 #xb7 #xf4 #xaa))))
              (get-uint32 p)))
(test-equal "B"
            (let ((p (open-bytevector-input-port #vu8(#x00 #x00 #x00 #x01 #x42))))
              (get-string p)))
(test-equal #x9a378f9b2e332a7
            (let ((p (open-bytevector-input-port
                      #vu8(#x00 #x00 #x00 #x08 #x09 #xa3 #x78 #xf9 #xb2 #xe3 #x32 #xa7))))
              (get-mpint p)))
(test-equal #x80
            (let ((p (open-bytevector-input-port
                      #vu8(#x00 #x00 #x00 #x02 #x00 #x80))))
              (get-mpint p)))
;; (test-equal -1234
;;             (let ((p (open-bytevector-input-port
;;                       #vu8(#x00 #x00 #x00 #x02 #xed #xcc))))
;;               (get-mpint p)))
;; (test-equal #x-deadbeef
;;             (let ((p (open-bytevector-input-port
;;                       #vu8(#x00 #x00 #x00 #x05 #xff #x21 #x52 #x41 #x11))))
;;               (get-mpint p)))
(test-end)

(test-begin "ssh-serial-put")
(test-equal #vu8(#x00 #x00 #x00 #x01 #x42)
            (call-with-bytevector-output-port
              (lambda (p)
                (put-bvstring p "B"))))
(test-equal #vu8(#x00 #x00 #x00 #x08 #x09 #xa3 #x78 #xf9 #xb2 #xe3 #x32 #xa7)
            (call-with-bytevector-output-port
              (lambda (p)
                (put-mpint p #x9a378f9b2e332a7))))
(test-equal #vu8(#x00 #x00 #x00 #x02 #x00 #x80)
            (call-with-bytevector-output-port
             (lambda (p)
               (put-mpint p #x80))))
;; (test-equal #vu8(#x00 #x00 #x00 #x02 #xed #xcc)
;;             (call-with-bytevector-output-port
;;               (lambda (p)
;;                 (put-mpint p #x-1234))))
;; (test-equal #vu8(#x00 #x00 #x00 #x05 #xff #x21 #x52 #x41 #x11)
;;             (call-with-bytevector-output-port
;;               (lambda (p)
;;                 (put-mpint p #x-deadbeef))))
(test-end)

(test-begin "ssh-serialize-get-zero")
(test-equal ""
            (let ((p (open-bytevector-input-port #vu8(#x00 #x00 #x00 #x00))))
              (get-string p)))
(test-equal #x0
            (let ((p (open-bytevector-input-port #vu8(#x00 #x00 #x00 #x00))))
              (get-mpint p)))
(test-end)

(test-begin "ssh-serialize-put-zero")
(test-equal #vu8(#x00 #x00 #x00 #x00)
            (call-with-bytevector-output-port
             (lambda (p)
               (put-string p #vu8()))))
(test-equal #vu8(#x00 #x00 #x00 #x00)
            (call-with-bytevector-output-port
             (lambda (p)
               (put-bvstring p #vu8()))))
(test-equal #vu8(#x00 #x00 #x00 #x00)
            (call-with-bytevector-output-port
             (lambda (p)
               (put-bvstring p ""))))
(test-equal #vu8(#x00 #x00 #x00 #x00)
            (call-with-bytevector-output-port
              (lambda (p)
                (put-mpint p 0))))
(test-end)

(test-exit)
