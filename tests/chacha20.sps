#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

(import (industria crypto chacha20)
        (industria tests runner)
        (srfi :64 testing)
        (rnrs))

(define (chacha20 key block-count nonce)
  (let ((output (make-bytevector 64)))
    (chacha20-block! output key block-count nonce)
    output))

;; Test vectors from RFC 7539

(test-begin "chacha20")

(define key
  #vu8(#x00 #x01 #x02 #x03 #x04 #x05 #x06 #x07
       #x08 #x09 #x0a #x0b #x0c #x0d #x0e #x0f
       #x10 #x11 #x12 #x13 #x14 #x15 #x16 #x17
       #x18 #x19 #x1a #x1b #x1c #x1d #x1e #x1f))

(define nonce
  #vu8(#x00 #x00 #x00 #x09 #x00 #x00 #x00 #x4a #x00 #x00 #x00 #x00))

(test-equal #vu8(#x10 #xf1 #xe7 #xe4 #xd1 #x3b #x59 #x15 #x50 #x0f #xdd #x1f #xa3 #x20 #x71 #xc4
                 #xc7 #xd1 #xf4 #xc7 #x33 #xc0 #x68 #x03 #x04 #x22 #xaa #x9a #xc3 #xd4 #x6c #x4e
                 #xd2 #x82 #x64 #x46 #x07 #x9f #xaa #x09 #x14 #xc2 #xd7 #x05 #xd9 #x8b #x02 #xa2
                 #xb5 #x12 #x9c #xd1 #xde #x16 #x4e #xb9 #xcb #xd0 #x83 #xe8 #xa2 #x50 #x3c #x4e)
            (chacha20 key 1 nonce))


(define plaintext-2
  #vu8(#x4c #x61 #x64 #x69 #x65 #x73 #x20 #x61 #x6e #x64 #x20 #x47 #x65 #x6e #x74 #x6c
       #x65 #x6d #x65 #x6e #x20 #x6f #x66 #x20 #x74 #x68 #x65 #x20 #x63 #x6c #x61 #x73
       #x73 #x20 #x6f #x66 #x20 #x27 #x39 #x39 #x3a #x20 #x49 #x66 #x20 #x49 #x20 #x63
       #x6f #x75 #x6c #x64 #x20 #x6f #x66 #x66 #x65 #x72 #x20 #x79 #x6f #x75 #x20 #x6f
       #x6e #x6c #x79 #x20 #x6f #x6e #x65 #x20 #x74 #x69 #x70 #x20 #x66 #x6f #x72 #x20
       #x74 #x68 #x65 #x20 #x66 #x75 #x74 #x75 #x72 #x65 #x2c #x20 #x73 #x75 #x6e #x73
       #x63 #x72 #x65 #x65 #x6e #x20 #x77 #x6f #x75 #x6c #x64 #x20 #x62 #x65 #x20 #x69
       #x74 #x2e))

(define ciphertext-2
  #vu8(#x6e #x2e #x35 #x9a #x25 #x68 #xf9 #x80 #x41 #xba #x07 #x28 #xdd #x0d #x69 #x81
       #xe9 #x7e #x7a #xec #x1d #x43 #x60 #xc2 #x0a #x27 #xaf #xcc #xfd #x9f #xae #x0b
       #xf9 #x1b #x65 #xc5 #x52 #x47 #x33 #xab #x8f #x59 #x3d #xab #xcd #x62 #xb3 #x57
       #x16 #x39 #xd6 #x24 #xe6 #x51 #x52 #xab #x8f #x53 #x0c #x35 #x9f #x08 #x61 #xd8
       #x07 #xca #x0d #xbf #x50 #x0d #x6a #x61 #x56 #xa3 #x8e #x08 #x8a #x22 #xb6 #x5e
       #x52 #xbc #x51 #x4d #x16 #xcc #xf8 #x06 #x81 #x8c #xe9 #x1a #xb7 #x79 #x37 #x36
       #x5a #xf9 #x0b #xbf #x74 #xa3 #x5b #xe6 #xb4 #x0b #x8e #xed #xf2 #x78 #x5e #x42
       #x87 #x4d))

(define key-2 #vu8(#x00 #x01 #x02 #x03 #x04 #x05 #x06 #x07
                   #x08 #x09 #x0a #x0b #x0c #x0d #x0e #x0f
                   #x10 #x11 #x12 #x13 #x14 #x15 #x16 #x17
                   #x18 #x19 #x1a #x1b #x1c #x1d #x1e #x1f))

(define nonce-2 #vu8(#x00 #x00 #x00 #x00 #x00 #x00 #x00 #x4a #x00 #x00 #x00 #x00))

(define keystream-2
  #vu8(#x22 #x4f #x51 #xf3 #x40 #x1b #xd9 #xe1 #x2f #xde #x27 #x6f #xb8 #x63 #x1d #xed #x8c #x13 #x1f #x82 #x3d #x2c #x06
       #xe2 #x7e #x4f #xca #xec #x9e #xf3 #xcf #x78 #x8a #x3b #x0a #xa3 #x72 #x60 #x0a #x92 #xb5 #x79 #x74 #xcd #xed #x2b
       #x93 #x34 #x79 #x4c #xba #x40 #xc6 #x3e #x34 #xcd #xea #x21 #x2c #x4c #xf0 #x7d #x41 #xb7 #x69 #xa6 #x74 #x9f #x3f
       #x63 #x0f #x41 #x22 #xca #xfe #x28 #xec #x4d #xc4 #x7e #x26 #xd4 #x34 #x6d #x70 #xb9 #x8c #x73 #xf3 #xe9 #xc5 #x3a
       #xc4 #x0c #x59 #x45 #x39 #x8b #x6e #xda #x1a #x83 #x2c #x89 #xc1 #x67 #xea #xcd #x90 #x1d #x7e #x2b #xf3 #x63))

(test-equal keystream-2
            (get-bytevector-n (chacha20-keystream key-2 1 nonce-2)
                              (bytevector-length keystream-2)))

(test-equal ciphertext-2
            (chacha20-encrypt plaintext-2 0 (bytevector-length plaintext-2)
                              (chacha20-keystream key-2 1 nonce-2)))

(test-equal plaintext-2
            (chacha20-encrypt ciphertext-2 0 (bytevector-length ciphertext-2)
                              (chacha20-keystream key-2 1 nonce-2)))

(test-end)

(test-exit)
